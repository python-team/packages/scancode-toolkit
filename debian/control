Source: scancode-toolkit
Section: devel
Priority: optional
Maintainer: Maximiliano Curia <maxy@debian.org>
Build-Depends: debhelper-compat (= 10), dh-python, python, python-setuptools
Standards-Version: 4.0.1
Homepage: https://github.com/nexB/scancode-toolkit
Vcs-Git: https://salsa.debian.org/python-team/packages/scancode-toolkit.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/scancode-toolkit

Package: scancode
Architecture: all
Depends: python, ${misc:Depends}, ${python:Depends}
Description: utilities to scan codebases for license and copyright
 ScanCode is a suite of utilities used to scan a codebase for license,
 copyright, packages dependencies and other interesting information that
 can be discovered in source and binary code files.
 .
 A typical software project often reuses hundreds of third-party
 components. License and origin information is often scattered and not
 easy to find: ScanCode discovers this data for you.
 .
 ScanCode provides reasonably accurate scan results and the line position
 where each result is found. The results can be formatted as JSON or
 HTML, and ScanCode provides a simple HTML app for quick visualization of
 scan results.
